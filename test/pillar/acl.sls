consul:
  # Start Consul agent service and enable it at boot time
  service: True

  version: 1.3.1

  config:
    server: True
    bind_addr: 0.0.0.0
    client_addr: 0.0.0.0
    enable_debug: True
    log_level: INFO
    datacenter: eu
    encrypt: "RIxqpNlOXqtr/j4BgvIMEw=="
    bootstrap_expect: 1
    acl_datacenter: eu
    acl_default_policy: deny
    acl_down_policy: 'extend-cache'
    acl_master_token: '4A3D4A92-E8B6-4167-8681-F22D521AAB3E'
    acl_agent_token: '830E8329-A665-4552-B160-E3FE782DEC0D'
    retry_interval: 15s
    node_name: test-consul
    addresses:
      # Allowing local connections for admin procedures
      http: '0.0.0.0'
    # retry_join:
    #   - 1.1.1.1
    #   - 2.2.2.2

  # The register key can be used to manually register apps
  register:
    - name: myapp
      token: 4A3D4A92-E8B6-4167-8681-F22D521AAB3E
      checks:
        - http: http://localhost:8500/v1/agent/members
          interval: 10s
        #    - name: Redis
        #      checks:
        #        - script: /usr/local/share/consul/check_redis.py
        #          interval: 10s
        #  scripts:
        #    - source: salt://files/consul/check_redis.py
        #      name: /usr/local/share/consul/check_redis.py

  # Add ACL Tokens
  acl_tokens:
    test1-token:
      token_name: "Test 1"
      token_type: client
      token_id: 707CBB61-C665-4EE4-9210-819BFEBF670A
      token_rules: '"node \"\" { policy = \"write\" } service \"\" { policy = \"read\" }"'
    test2-token:
      token_name: "Test 2"
      token_type: client
      token_id: 41D1FBBF-26E4-4B00-B944-B934D4261B91
      token_rules: '"node \"\" { policy = \"write\" } service \"\" { policy = \"read\" }"'

# Fill keyvalue store with data
  kvstore:
    foo:
      bar:
        rocks: 'forever'
